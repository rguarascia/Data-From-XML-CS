﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Gettings_Data_From_XML_File
{
    class Program
    {
        static void Main(string[] args)
        {
            //XML File out

            XmlWriter XMLFileToFile = null;

            try
            {
                XmlWriterSettings XMLSettings = new XmlWriterSettings();

                XMLSettings.Indent = true;
                XMLSettings.IndentChars = ("\t");
                XMLSettings.OmitXmlDeclaration = true;

                XMLFileToFile = XmlWriter.Create("infile.xml", XMLSettings);
                XMLFileToFile.WriteStartElement("Parent");
                XMLFileToFile.WriteElementString("Child", "Data");
                XMLFileToFile.WriteEndElement();

                XMLFileToFile.Flush();
            }
            finally
            {
                if (XMLFileToFile != null)
                {
                    XMLFileToFile.Close();
                }
            }

            XmlReader ReadXML = XmlReader.Create("infile.xml"); //Creates XMLReader instance

            while (ReadXML.NodeType != XmlNodeType.EndElement) //Sets the node types to closes. EI </>
            {
                ReadXML.Read(); //Reads the XML Doc
                if (ReadXML.Name == "Child") //Focuses node 
                {
                    while (ReadXML.NodeType != XmlNodeType.EndElement) //Sets it to close element again 
                    {
                        ReadXML.Read();
                        if (ReadXML.NodeType == XmlNodeType.Text) //Gets the text in the node
                        {
                            Console.WriteLine("In 'Child' node = {0}", ReadXML.Value);
                            Console.ReadKey();
                        }
                    }
                }
            }
        }
    }
}
